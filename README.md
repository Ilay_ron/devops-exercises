# DevOps-Exercises
Building package for Redhat system
Create a script that receives options -v --version -n --name
    - if -v or --version is given it should print out current version of the script
    - if -n or --name is given it should return string 'hello NAME' with provided name instead of NAME
    - if no option is given the string 'Hello stranger' should be return
write a test with BATS to verify that the script suits all the provided conditions above.
create rpm package of the script to be installed on the system
create a pipeline to deploy script, test,packaging of rpm and deploying of that rpm to Redhat based system
